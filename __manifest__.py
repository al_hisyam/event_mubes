# -*- coding: utf-8 -*-
{
    'name': "event_mubes",

    'summary': """
        """,

    'description': """
        Long description of module's purpose
    """,

    'author': "Programmer Surabaya",
    'website': "http://www.programmersurabaya.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/openerp/addons/base/module/module_data.xml
    # for the full list
    'category': 'event',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'event', 'barcodes', 'organisasi'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/event_views.xml',
        'views/event_barcode.xml',
        'reports/report_event.xml',
        'wizard/barcode_scan.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'application': True,
    'installable': True,
    'qweb': ['static/src/xml/picking.xml'],
    'auto_install': False,
}