# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime
from odoo.exceptions import ValidationError

class EventCommitteeType(models.Model):
    _name = 'event.committee.type'
    _description = 'Jabatan Panitia'

    name = fields.Char('Nama Jabatan')
    description = fields.Char('Description')
    parent_id = fields.Many2one('event.committee.type', string='Atasan')

class EventCommitte(models.Model):
    _name = 'event.committee'
    _description = 'Committee'

    event_id = fields.Many2one('event.event', string='Event')
    committee_type_id = fields.Many2one('event.committee.type', string='Jabatan Panitia')
    employee_id = fields.Many2one('hr.employee', string='Panitia')

class EventEvent(models.Model):
    _description = 'Event'
    _inherit = 'event.event'

    committee_ids = fields.One2many('event.committee', inverse_name='event_id', string='Panitia')
    room_attendee_ids = fields.One2many('event.hotel.room.attendee', inverse_name='event_id', string='Kamar hotel')
    address = fields.Char('Alamat')
    kelas_lokakarya_ids = fields.One2many('event.kelas.lokakarya', inverse_name='event_id', string='Kelas Lokakarya')
    sub_event_ids = fields.One2many('event.sub.event', inverse_name='event_id', string='Sub Event')
    atteendee_ids = fields.One2many('event.registration', inverse_name='event_id', string='Peserta')
    date_display_barcode = fields.Char('Date Format', compute='_compute_date_display_barcode')

    def event_formatted_date(self):
        bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        date_begin = datetime.datetime.strptime(self.date_begin, "%Y-%m-%d %H:%M:%S").date()
        date_end = datetime.datetime.strptime(self.date_end, "%Y-%m-%d %H:%M:%S").date()
        return '%s - %s  %s' % (date_begin.strftime("%d"), date_end.strftime("%d"), bulan[int(date_begin.strftime("%-m"))])

    def _compute_date_display_barcode(self):
        bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
        for event in self:
            date_begin = datetime.datetime.strptime(event.date_begin, "%Y-%m-%d %H:%M:%S").date()
            date_end = datetime.datetime.strptime(event.date_end, "%Y-%m-%d %H:%M:%S").date()
            day_begin = date_begin.strftime("%d")
            day_end = date_end.strftime("%d")
            month = bulan[int(date_begin.strftime("%-m"))]
            event.date_display_barcode = '%s - %s  %s' % (day_begin, day_end, month)

    @api.multi
    def open_session_cb(self):
        id = self.id
        return {
            'type': 'ir.actions.act_url',
            'target': 'self',
            'res_id': self.id,
            'url': "/barcode/web/%s" %id,
        }

class EventRegistration(models.Model):
    _name = 'event.registration'
    _inherit = 'event.registration'

    komisaris_id = fields.Many2one('organisasi.komisaris', 'Komisaris')
    pengurus_daerah_id = fields.Many2one('organisasi.pengurus.daerah', 'Pengurus Daerah', domain="[('komisaris_id', '=', komisaris_id)]")
    partner_id = fields.Many2one('res.partner', string='Anggota', required=True)
    accommodation = fields.Selection([('pribadi', 'Pribadi'), ('panitia', 'Panitia')], string='Akomodasi', default='pribadi')
    transportation = fields.Selection([('pribadi', 'Pribadi'), ('panitia', 'Bus Panitia'), ('mobil_panitia', 'Mobil Panitia')], string='Transportasi', default='pribadi')
    hotel_id = fields.Many2one('event.hotel', string='Hotel')
    room_type_id = fields.Many2one('event.hotel.room.type', string="Tipe Kamar")
    room_id = fields.Many2one('event.hotel.room', string='No Kamar')
    breakfast = fields.Boolean('Makan Pagi')
    lunch = fields.Boolean('Makan Siang')
    dinner = fields.Boolean('Makan Malam')
    sub_event_ids = fields.One2many('event.sub.event.attendee', inverse_name='attendee_id', string='Sub Acara')
    kelas_lokakarya_id = fields.Many2one('event.kelas.lokakarya', string='Kelas Lokakarya')
    tanggal_konfirmasi = fields.Date('Tanggal Konfirmasi')
    no_keanggotaan = fields.Char('No Keanggotaan')

    def _compute_no_anggota(self):
        for partner in self:
            partner.no_keanggotaan = partner.partner_id.no_keanggotaan

    @api.multi
    def action_save_no_keanggotaan(self):
        for row in self:
            row.write({'no_keanggotaan': row.partner_id.no_keanggotaan})
        return self

    @api.model
    def create(self, vals):
        record = super(EventRegistration, self).create(vals)
        self.create_sub_event(record.id, record.event_id)
        return record

		
    @api.onchange('partner_id')
    def _onchange_partner(self):
        if self.partner_id:
            self.name = self.partner_id.name
            self.email = self.partner_id.email
            self.phone = self.partner_id.phone
            self.no_keanggotaan = self.partner_id.no_keanggotaan
    
    @api.onchange('department_id')
    def _onchange_department(self):
        if self.department_id:
            department_id = self.department_id.name
            self.name = department_id.name or self.name

    @api.onchange('accommodation')
    def _onchange_accommodation(self):
        if self.accommodation == 'panitia':
            self.breakfast = True
            self.lunch = True
            self.dinner = True
        else:
            self.breakfast = False
            self.lunch = False
            self.dinner = False
            self.transportation = 'pribadi'

    def create_sub_event(self, attendee_id, event_id):
        sub_events = self.env['event.sub.event'].sudo().search([
            ['event_id', '=',  event_id.id]
            ])
        for sub_event in sub_events:
            self.env['event.sub.event.attendee'].create(
                    {
                        'sub_event_id': sub_event.id,
                        'attendee_id': attendee_id,
                    }
                )
    
class EventHotel(models.Model):
    _name = 'event.hotel'
    _description = 'Hotel'

    name = fields.Char('Nama Hotel')
    address = fields.Char('Alamat')
    room_ids = fields.One2many('event.hotel.room', inverse_name="event_hotel_id", string="Kamar")
    room_type_ids = fields.One2many('event.hotel.room.type', inverse_name="event_hotel_id", string="Tipe Kamar")

class EventHotelRoom(models.Model):
    _name = 'event.hotel.room'
    _description = 'Kamar Hotel'

    name = fields.Char('No Kamar')
    event_hotel_id = fields.Many2one('event.hotel', string='Hotel')
    event_hotel_room_type_id = fields.Many2one('event.hotel.room.type', string='Tipe Kamar Hotel')

class EventHotelRoomType(models.Model):
    _name = 'event.hotel.room.type'
    _description = 'Tipe Kamar Hotel'

    event_hotel_id = fields.Many2one('event.hotel', string='Hotel')
    name = fields.Char('Nama Kamar')
    display_name = fields.Char(compute='_compute_display_name', store=True, index=True)
    bed_total = fields.Integer('Jumlah Bed')

    @api.depends('name')
    def _compute_display_name(self):
        for partner in self:
            partner.display_name = partner.name + "   (" + partner.event_hotel_id.name + ")"

class EventHotelRoomAttendee(models.Model):
    _name = 'event.hotel.room.attendee'
    _description = 'Kamar Hotel Peserta'

    attendee1_id = fields.Many2one('event.registration', string='Peserta 1')
    attendee2_id = fields.Many2one('event.registration', string='Peserta 2')
    event_id = fields.Many2one('event.event', string='Event')
    hotel_id = fields.Many2one('event.hotel', string='Hotel')
    room_type_id = fields.Many2one('event.hotel.room.type', string="Tipe Kamar")
    room_id = fields.Many2one('event.hotel.room', string='No Kamar')

    @api.model
    def create(self, vals):
        rooms = self.env['event.hotel.room.attendee'].sudo().search([
                ['event_id', '=', vals['event_id']],
                ['hotel_id', '=', vals['hotel_id']],
                ['room_id', '=', vals['room_id']]
            ])
        if len(rooms) > 0:
                raise ValidationError("Hotel sudah digunakan") 
        record = super(EventHotelRoomAttendee, self).create(vals)
        return record

    @api.model
    def write(self, vals):
        if 'room_id' in vals:
            rooms = self.env['event.hotel.room.attendee'].sudo().search([
                    ['event_id', '=', self.event_id.id],
                    ['hotel_id', '=', self.hotel_id.id],
                    ['room_id', '=', vals['room_id']]
                ])
            if len(rooms) > 0:
                    raise ValidationError("Kamar hotel sudah digunakan") 
        record = super(EventHotelRoomAttendee, self).write(vals)
        return record

class EventSubEvent(models.Model):
    _name = 'event.sub.event'
    _description = 'Sub Acara'

    event_id = fields.Many2one('event.event', string='Event')
    name = fields.Char('Name')
    date_begin = fields.Datetime(string='Start Date', required=True, track_visibility='onchange')
    date_end = fields.Datetime(string='End Date', required=True, track_visibility='onchange')
    information = fields.Char('Keterangan')

class EventSubEventAttendee(models.Model):
    _name = 'event.sub.event.attendee'
    _description = 'Sub Acara Peserta'

    sub_event_id = fields.Many2one('event.sub.event', string='Sub Event')
    attendee_id = fields.Many2one('event.registration', string='Anggota')

class EventKelasLokakarya(models.Model):
    _name = 'event.kelas.lokakarya'
    _description = 'Kelas Lokakarya'

    event_id = fields.Many2one('event.event', string='Event')
    name = fields.Char('Name')
    information = fields.Char('Keterangan')
