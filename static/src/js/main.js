
odoo.define('point_of_sale.main', function (require) {
"use strict";

var barcode = require('event_mubes.barcode');
var core = require('web.core');

core.action_registry.add('event_mubes.ui', barcode.BarcodeEventRegister);

});
