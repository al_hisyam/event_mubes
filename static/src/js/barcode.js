odoo.define('event_mubes.barcode', function (require) {
'use strict';

    var core = require('web.core');
    var ajax = require('web.ajax');
    var CrashManager = require('web.CrashManager');

    var _t = core._t;
    var QWeb = core.qweb;
    var utils = require('web.utils');
    var Widget = require('web.Widget');
    var Model = require('web.Model');

    var exports = {}

    var session = require('web.session');
    var Backbone = window.Backbone;
    var mixins = core.mixins;
    var BarcodeEvents = require('barcodes.BarcodeEvents');
    var BarcodeHandlerMixin = require('barcodes.BarcodeHandlerMixin');

    var BarcodeEventRegister = Widget.extend({
        template: "HomePageTemplate",
        //className: 'o_content',
        init:function(parent,options){
            this._super(parent);
            options = options || {};
            this.refreshEvent();
            this.bindBarcode();
            //BarcodeEvents.BarcodeEvents.start()
            console.log(BarcodeEvents)
        },
        start: function () {
            var sup = this._super();
            $('.o_content').append(this.$el);
        },
        events: {
            'click button.back_button': function () {
               window.history.back();
            }
        },
        refreshEvent: function(){
            self = this;
            var Event = new Model('event.event');
            Event.query(['id', 'display_name', 'name', 'company_id', 'date_display_barcode', 'seats_used', 'date_begin', 'user_id', 'date_end', 'seats_expected', 'address_id'])
                 .filter([['id', '=', odoo.event]])
                 .first().then(function (event) {
                    self.$('.o_event_total_attendee').text(event.seats_expected)
                    self.$('.o_event_reg_attendee').text(event.seats_used)
                    self.$('.o_event_title').text(event.name)
                    self.$('.o_event_day').text(event.date_display_barcode)
                    self.$('.barcode_input').val("");
            });
        },
        getEventRegistration:function(barcode){
            if(barcode === '')
                return;

            self = this;
            var Attendee = new Model('res.partner');
            Attendee.query(['id', 'no_keanggotaan', 'name'])
                 .filter([['no_keanggotaan', '=', barcode]])
                 .first().then(function (attendee) {
                    if(attendee != null){
                        self.findEventRegistration(attendee);
                        //self.$('.mt16').attr('src', attendee.name);
                        self.$('.o_event_attendee_name').html(attendee.name);
                    }else{
                         alert('Peserta tidak ditemukan')
                         self.$('.barcode_input').val('');
                    }
                 })
        },

        findEventRegistration: function(attendee){
            self = this;
        
            var EventRegistration = new Model('event.registration');
            EventRegistration.query(['id', 'event_id', 'state', 'partner_id', 'name'])
                 .filter([['event_id', '=', odoo.event], ['partner_id', '=', attendee.id]])
                 .first().then(function (event_registration) {
                   if(event_registration == null){
                        alert("Peserta tidak terdaftar pada acara ini!")
                        return;
                    }
                    console.log(event_registration)
                    self.confirmAttendance(EventRegistration, event_registration);
                 })
        },
        confirmAttendance: function(EventRegistration, event_registration){
            self = this;
            if(event_registration === null){
                alert('Peserta tidak ditemukan')
            }else if(event_registration.state === 'done'){
                self.$('.barcode_input').val("");
                alert(event_registration.partner_id[1] + ' sudah terdaftar dan hadir')
            }else{
                EventRegistration.call('button_reg_close', [event_registration.id])
                    .then(function (event) {
                        alert(event_registration.partner_id[1] + " hadir di acara " + event_registration.event_id[1]);
                        self.refreshEvent();
                     });
            }
        },
        bindBarcode: function(){
            self = this;
            var barcode = "";
            self.$('.barcode_input').focus();
            
            $(document).keydown(function(e) {

                var code = (e.keyCode ? e.keyCode : e.which);
                if(code==13)// Enter key hit
                {
                    barcode = self.$('.barcode_input').val();
                    self.getEventRegistration(barcode);
                    barcode = "";
                }
                else if(code==9)// Tab key hit
                {
                    barcode = "";
                }
                else if(code==32)// Tab key hit
                {
                    //self.$('.barcode_input').val(barcode)
                    barcode = "";
                }
                else
                {
                    barcode = barcode + String.fromCharCode(code);
                }
            });
        }
    });

    return {
        BarcodeEventRegister: BarcodeEventRegister
    };
})