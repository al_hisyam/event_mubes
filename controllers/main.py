# -*- coding: utf-8 -*-
import logging

import json
from odoo import http
from odoo.http import request

_logger = logging.getLogger(__name__)

class BarcodeController(http.Controller):

    @http.route(['/barcode/web/<event_id>'], type='http', auth='user')
    def a(self, event_id, debug=False, **k):
        if not request.session.uid:
            return http.local_redirect('/web/login?redirect=/barcode/web')

        context = {
            'session_info': json.dumps(request.env['ir.http'].session_info()),
            'event': event_id
        }
        return request.render('event_mubes.barcode_index', qcontext=context)