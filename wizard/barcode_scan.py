# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import UserError


class BarcodeScanWizard(models.TransientModel):
    _name = 'barcode.scan.wizard'

    event_id = fields.Many2one('event.event', string='Event')